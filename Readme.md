# Simple live price UI

This system interfaces to a backend service that is written in Python/Pandas.

It reads live stock prices from yahoo finance using the pandas-datareader library.

The UI here sends HTTP GET requests to this backend service, and processes the resulting json for display.

Contact your instructor if you have any questions about integrating something similar into your application.